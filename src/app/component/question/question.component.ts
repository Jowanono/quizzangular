import {Component, Input, OnInit} from '@angular/core';
import {ApiJavaService} from '../../service/apiJava.service';
import {QuestionModel} from '../../model/question.model';
import {ActivatedRoute, Router} from '@angular/router';
import {AnswerModel} from '../../model/answer.model';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  question: QuestionModel;
  id: string;
  userAnswer: string;
  checkedAnswer: string;
  hidden = false;
  error: string;

  constructor(
    private apiService: ApiJavaService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.apiService.getQuestion(this.id).subscribe(q => {
        console.log(q);
        this.question = q;
      }, error => {
        console.log(error);
        this.error = error;
      });
    });
    if (!this.id) {
      this.apiService.getQuestion('4').subscribe(q => {
        console.log(q);
        this.question = q;
      });
    }
  }

  clicked(id: string, answer: string) {
    this.hidden = true;
    this.apiService.tryAnswer(id, new AnswerModel(answer)).subscribe(reponse => {
        console.log(reponse);
        if (reponse.result === 'CORRECT') {
           this.checkedAnswer = 'Bonne réponse!!!';
        } else if (reponse.result === 'INCORRECT') {
          this.checkedAnswer = 'Mauvaise réponse...';
        } else {
          this.checkedAnswer = 'Presque! Essaie encore!';
        }
    });
  }

  change() {}
}
