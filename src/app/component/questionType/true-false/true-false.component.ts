import {Component, Input, OnInit} from '@angular/core';
import {ApiJavaService} from '../../../service/apiJava.service';
import {QuestionToCreateModel} from '../../../model/questionToCreate.model';

@Component({
  selector: 'app-true-false',
  templateUrl: './true-false.component.html',
  styleUrls: ['./true-false.component.css']
})
export class TrueFalseComponent implements OnInit {
  enonce: string;
  reponse: boolean;
  creationValidation: string;

  constructor(
    private apiService: ApiJavaService,
  ) {
  }

  ngOnInit() {
  }

  repondre(reponse: boolean) {
    this.reponse = reponse;
  }

  generateTrueFalseQ() {
    const trueFalseQuestionF = new QuestionToCreateModel('TRUE_FALSE', this.enonce, null, this.reponse);
    this.apiService.createQuestion(trueFalseQuestionF).subscribe(oq => {
      console.log(oq);
      this.creationValidation = `Votre question a bien été générée! Retrouvez là dans l'onglet 'Toutes les questions'`;
    });
  }

}
