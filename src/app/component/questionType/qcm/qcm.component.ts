import {Component, Input, OnInit} from '@angular/core';
import {QuestionToCreateModel} from '../../../model/questionToCreate.model';
import {ApiJavaService} from '../../../service/apiJava.service';

@Component({
  selector: 'app-qcm',
  templateUrl: './qcm.component.html',
  styleUrls: ['./qcm.component.css']
})
export class QCMComponent implements OnInit {
  @Input() reponse: string;
  @Input() enonce: string;
  @Input() suggestion1: string;
  @Input() suggestion2: string;
  @Input() suggestion3: string;
  suggestionList: string [];
  creationValidation: string;

  constructor(
    private apiService: ApiJavaService) { }

  ngOnInit() {
  }

  generateQCMQ() {
    this.suggestionList = [this.suggestion1, this.suggestion2, this.suggestion3];
    const qcm = new QuestionToCreateModel('MULTIPLE', this.enonce, this.suggestionList, this.reponse);
    this.apiService.createQuestion(qcm).subscribe(oq => {
      console.log(oq);
      this.creationValidation = `Votre question a bien été générée! Retrouvez là dans l'onglet 'Toutes les questions'`;
    });
  }
}
