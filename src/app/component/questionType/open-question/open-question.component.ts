import {Component, OnInit} from '@angular/core';
import {QuestionToCreateModel} from '../../../model/questionToCreate.model';
import {ApiJavaService} from '../../../service/apiJava.service';

@Component({
  selector: 'app-open-question',
  templateUrl: './open-question.component.html',
  styleUrls: ['./open-question.component.css']
})
export class OpenQuestionComponent implements OnInit {
  enonce: string;
  reponse: string;
  creationValidation: string;

  constructor(
    private apiService: ApiJavaService
  ) {
  }

  ngOnInit() {
  }

  generateOpenQ() {
    const openQuestion = new QuestionToCreateModel('OPEN', this.enonce, null, this.reponse);
    this.apiService.createQuestion(openQuestion).subscribe(oq => {
      console.log(oq);
      this.creationValidation = `Votre question a bien été générée! Retrouvez là dans l'onglet 'Toutes les questions'`;
    });
  }
}
