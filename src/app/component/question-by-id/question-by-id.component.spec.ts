import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionByIdComponent } from './question-by-id.component';

describe('QuestionByIdComponent', () => {
  let component: QuestionByIdComponent;
  let fixture: ComponentFixture<QuestionByIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionByIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionByIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
