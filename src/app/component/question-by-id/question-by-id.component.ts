import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-question-by-id',
  templateUrl: './question-by-id.component.html',
  styleUrls: ['./question-by-id.component.css']
})
export class QuestionByIdComponent implements OnInit {
  @Input() id: string;
  hidden = true;

  constructor() { }

  ngOnInit() {
  }

  change() {

  }

  clicked() {
    this.hidden = false;
  }
}
