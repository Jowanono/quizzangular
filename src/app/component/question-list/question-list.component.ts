import {Component, Input, OnInit} from '@angular/core';
import {QuestionModel} from '../../model/question.model';
import {ApiJavaService} from '../../service/apiJava.service';

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.css']
})
export class QuestionListComponent implements OnInit {
  @Input() questionList: QuestionModel[] = [];

  constructor(
    private apiService: ApiJavaService
  ) {
  }

  ngOnInit() {
    this.apiService.getQuestionList().subscribe(qList => {
      console.log(qList);
      this.questionList = qList;
    });
  }
}
