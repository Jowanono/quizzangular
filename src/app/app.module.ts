import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {QuestionComponent} from './component/question/question.component';
import {QuestionListComponent} from './component/question-list/question-list.component';
import {QuestionGeneratorComponent} from './component/question-generator/question-generator.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {ApiJavaService} from './service/apiJava.service';
import {FormsModule} from '@angular/forms';
import { OpenQuestionComponent } from './component/questionType/open-question/open-question.component';
import { TrueFalseComponent } from './component/questionType/true-false/true-false.component';
import { QCMComponent } from './component/questionType/qcm/qcm.component';
import { QuestionByIdComponent } from './component/question-by-id/question-by-id.component';
import { ProfilComponent } from './component/profil/profil.component';
import { GreetingsComponent } from './component/greetings/greetings.component';

const appRoutes: Routes = [
  { path: 'profil', component: ProfilComponent },
  { path: 'question', component: QuestionByIdComponent },
  { path: 'question/:id', component: QuestionComponent },
  { path: 'questionList', component: QuestionListComponent },
  { path: 'questionGenerator', component: QuestionGeneratorComponent },
  { path: 'openQuestion', component: OpenQuestionComponent },
  { path: 'trueFalse', component: TrueFalseComponent },
  { path: 'QCM', component: QCMComponent },
  { path: '', component: GreetingsComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    QuestionComponent,
    QuestionListComponent,
    QuestionGeneratorComponent,
    OpenQuestionComponent,
    TrueFalseComponent,
    QCMComponent,
    QuestionByIdComponent,
    ProfilComponent,
    GreetingsComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule,
    HttpClientModule,
    RouterModule,
    FormsModule
  ],
  providers: [
    ApiJavaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
