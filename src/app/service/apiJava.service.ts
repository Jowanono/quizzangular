import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {QuestionModel} from '../model/question.model';
import {AnswerModel} from '../model/answer.model';
import {ApiAnswerModel} from '../model/apiAnswer.model';
import {QuestionToCreateModel} from '../model/questionToCreate.model';

@Injectable()

export class ApiJavaService {

  constructor(
    private http: HttpClient
  ) {
  }
  getQuestion(id: string): Observable<QuestionModel> {
    return this.http.get<QuestionModel>('http://localhost:8080/questions/' + id);
  }

  getQuestionList(): Observable<QuestionModel []> {
    return this.http.get<QuestionModel []>('http://localhost:8080/questions');
  }

  tryAnswer(id: string, userAnswer: AnswerModel): Observable<ApiAnswerModel> {
    return this.http.post<ApiAnswerModel>('http://localhost:8080/questions/' + id + '/answer',
      userAnswer);
  }

  createQuestion(createdQuestion: QuestionToCreateModel) {
    return this.http.post('http://localhost:8080/questions', createdQuestion);
  }
}
