export class QuestionModel {
  id: string;
  type: string;
  enonce: string;
  suggestions: string [];
  reponse: object;

  constructor(
    id: string,
    type: string,
    enonce: string,
    suggestions: string [],
    reponse: object
  ) {
    this.id = id;
    this.type = type;
    this.enonce = enonce;
    this.suggestions = suggestions;
    this.reponse = reponse;
  }
}
