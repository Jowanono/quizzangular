export class QuestionToCreateModel {
  type: string;
  enonce: string;
  suggestions: string [];
  reponse: string | boolean;

  constructor(
    type: string,
    enonce: string,
    suggestions: string [],
    reponse: string | boolean
  ) {
    this.type = type;
    this.enonce = enonce;
    this.suggestions = suggestions;
    this.reponse = reponse;
  }


}
